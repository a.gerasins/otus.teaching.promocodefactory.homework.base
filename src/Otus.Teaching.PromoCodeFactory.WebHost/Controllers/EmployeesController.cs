﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <param name="employeeShortRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeShortResponse>> CreateEmployeeAsync([FromBody] EmployeeShortRequest employeeShortRequest)
        {
            if (employeeShortRequest == null) 
                return BadRequest("Could not create employee with null value");

            var employee = _mapper.Map<Employee>(employeeShortRequest);
            var newEmployee = await _employeeRepository.CreateAsync(employee);

            return _mapper.Map<EmployeeShortResponse>(newEmployee);
        }
        
        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeShortRequest"></param>
        /// <returns></returns>
        [HttpPatch("{id:guid}")]
        public async Task<ActionResult<EmployeeShortResponse>> UpdateAsync(Guid id, [FromBody] EmployeeShortRequest employeeShortRequest)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            _mapper.Map(employeeShortRequest, employee);
            await _employeeRepository.UpdateAsync(employee);
            return _mapper.Map<EmployeeShortResponse>(employee);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (await _employeeRepository.DeleteAsync(id))
                return Ok();
            return NotFound();
        }

    }
}