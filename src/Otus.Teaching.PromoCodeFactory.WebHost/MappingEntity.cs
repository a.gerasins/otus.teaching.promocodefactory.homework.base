﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class MappingEntity: Profile
    {
        public MappingEntity()  
        {  
            CreateMap<EmployeeShortRequest, Employee>(); 
            CreateMap<Employee, EmployeeShortResponse>(); 
        }
    }
}