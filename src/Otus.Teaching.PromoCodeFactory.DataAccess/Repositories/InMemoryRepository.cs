﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private List<T> Data { get; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            if (data == null) 
                throw new NullReferenceException(nameof(data));
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        
        public Task<T> CreateAsync(T entity)
        {
            if (entity == null) 
                throw new ArgumentNullException(nameof(entity));
            
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            
            return Task.FromResult(entity);
        }

        public Task<T> UpdateAsync(T entity)
        {
            if (entity == null) 
                throw new ArgumentNullException(nameof(entity));

            var data = Data.FirstOrDefault(x => x.Id == entity.Id);
            
            if (data == null) 
                throw new Exception($"Entity {entity.Id} does not found");

            data = entity;
            return Task.FromResult(data);
        }
        
        public Task<bool> DeleteAsync(Guid id)
        {
            return Task.FromResult(Data.RemoveAll(x => x.Id == id) > 0);
        }
    }
}